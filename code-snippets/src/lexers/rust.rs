use logos::Logos;

//FIXME: regex for symbols and update list of symbols
#[derive(Debug, PartialEq, Logos)]
pub enum Token {
	#[regex("///[^\n]*\n?")]
	DocComment,

	#[regex("//[^\n]*\n?")]
	Comment,

	#[token("///\n")]
	BlankDocComment,

	#[token("//\n")]
	BlankComment,

	#[token("use")]
	Use,

	#[token("pub")]
	Pub,

	#[token("fn")]
	Fn,

	#[token("struct")]
	Struct,

	#[token("trait")]
	Trait,

	#[token("let")]
	Let,

	// macro call
	#[regex("_?[a-z][a-z0-9_]*!")]
	MacroCall,

	#[regex(r#""(\\.|[^"\\])*""#)]
	LireralStr,

	// structs, enums or trait definitions
	#[regex("_?[A-Z][A-Za-z0-9]*")]
	TypeIdent,

	// idents
	#[regex("_?[a-z][a-z0-9_]*")]
	Ident,

	#[regex("[1-9][_1-9]*")]
	LireralDecimal,

	#[regex("[1-9][_1-9]*.")]
	LireralFloat,

	// lifetimes
	#[regex("'_?[a-z][a-z0-9_]*")]
	TickIdent,

	#[token("(")]
	OpenParen,

	#[token(")")]
	CloseParen,

	#[token("{")]
	OpenBrace,

	#[token("}")]
	CloseBrace,

	#[token("[")]
	OpenBracket,

	#[token("]")]
	CloseBracket,

	#[token("<")]
	Less,

	#[token(">")]
	Greater,

	#[token("#")]
	Sharp,

	#[token(".")]
	Dot,

	#[token("=")]
	Equals,

	#[token(";")]
	Semi,

	#[token(",")]
	Comma,

	#[token("::")]
	DoubleColon,

	#[token(":")]
	Colon,

	#[token("->")]
	ThinArrow,

	#[token("&")]
	Ampersand,

	#[token("!")]
	Bang,

	#[token("|")]
	Pleca,

	#[token("_")]
	Underscore,

	#[token(" ")]
	Space,

	#[token("\t")]
	Tab,

	//this fails but is useful to keep breaklines in html
	#[token("\n")]
	Breakline,

	#[token("/", priority = 3)]
	Slash,

	#[token("\\")]
	Backslash,
}

impl Token {
	pub fn is_space(&self) -> bool {
		matches!(self, Token::Space)
	}

	pub fn is_breakline(&self) -> bool {
		matches!(self, Token::BlankDocComment)
			|| matches!(self, Token::BlankComment)
			// || matches!(self, Token::Breakline)
	}

	pub fn css_class(&self) -> &str {
		match self {
			Token::Use | Token::Pub | Token::Fn | Token::Struct | Token::Trait | Token::Let => {
				"keyword"
			}
			Token::LireralStr => "literal-str",
			Token::TypeIdent => "type-ident",
			Token::MacroCall => "macro-call",
			Token::Ident => "ident",
			Token::TickIdent => "lifetime",
			Token::Ampersand => "ampersand ",
			Token::Dot
			| Token::Sharp
			| Token::OpenParen
			| Token::CloseParen
			| Token::OpenBrace
			| Token::CloseBrace
			| Token::OpenBracket
			| Token::CloseBracket
			| Token::Less
			| Token::Greater
			| Token::Equals
			| Token::Semi
			| Token::Comma
			| Token::DoubleColon
			| Token::Colon
			| Token::ThinArrow
			| Token::Bang
			| Token::Pleca
			| Token::Underscore
			| Token::Slash
			| Token::Backslash => "symbol",
			Token::Space => "sapce",
			Token::Breakline => "breakline",
			Token::Tab => "tab",
			Token::LireralFloat | Token::LireralDecimal => "literal-number",
			Token::DocComment | Token::Comment => "comment",
			Token::BlankComment | Token::BlankDocComment => "comment-breakline",
		}
	}
}
