use m3rs::{core::containers::*, yew::color::ColorRole};
use wasm_bindgen::JsCast;
use web_sys::{HtmlElement, HtmlStyleElement};

pub(crate) mod builders;
pub(crate) mod lexers;
pub(crate) mod utils;

#[derive(Debug, Clone, Copy)]
pub enum Language {
	Rust,
}

#[derive(Debug, Clone, Copy)]
pub struct Input<'input> {
	pub lang: Language,
	pub code: &'input str,
}

pub fn code_snnipet(input: Input<'_>) -> HtmlElement {
	let code = match input.lang {
		Language::Rust => builders::rust::build(input.code),
	};

	let container: HtmlElement = Container::new(
		code,
		ContainerOpts {
			scale: Scale::Medium,
			color: ColorRole::None,
			..Default::default()
		},
	);

	let style = web_sys::window()
		.unwrap()
		.document()
		.unwrap()
		.create_element("style")
		.unwrap()
		.dyn_into::<HtmlStyleElement>()
		.unwrap();

	style.set_inner_html(include_str!(concat!(env!("OUT_DIR"), "/index.css")));

	container
		.shadow_root()
		.unwrap()
		.append_child(&style)
		.unwrap();

	container.style().set_property("width", "100%").unwrap();

	let component_container = utils::create_html_element("div");

	let comp_style = component_container.style();
	comp_style.set_property("overflow", "scroll").unwrap();
	comp_style.set_property("width", "fit-content").unwrap();
	comp_style.set_property("max-width", "100%").unwrap();

	component_container.append_child(&container).unwrap();

	component_container
}
