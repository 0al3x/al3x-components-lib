use wasm_bindgen::JsCast;
use web_sys::{Document, Element, HtmlElement};

pub fn document() -> Document {
	web_sys::window().unwrap().document().unwrap()
}

pub fn create_element(name: &str) -> Element {
	document().create_element(name).unwrap()
}

pub fn create_html_element(name: &str) -> HtmlElement {
	create_element(name).dyn_into().unwrap()
}

// pub fn create_element_as<T: JsCast>(name: &str) -> T {
// 	create_element(name).dyn_into::<T>().unwrap()
// }
