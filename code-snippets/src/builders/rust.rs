use crate::lexers::rust::Token;
use crate::utils;
use logos::Logos;
use web_sys::HtmlElement;

pub fn build(code: &str) -> HtmlElement {
	let container = utils::create_html_element("div");
	container.set_class_name("code-container");

	let mut lexer = Token::lexer(code);

	while let Some(tok) = lexer.next() {
		match tok {
			Ok(t) => {
				if t.is_space() {
					container.append_child(&space()).unwrap();
				} else if t.is_breakline() {
					container.append_child(&breakline()).unwrap();
					container.append_child(&breakline()).unwrap();
				} else {
					container.append_child(&word(lexer.slice(), &t)).unwrap();
				}
			}
			Err(_) => {
				container.append_child(&breakline()).unwrap();
			}
		}
	}

	container
}

fn space() -> HtmlElement {
	let span = utils::create_html_element("span");
	span.set_inner_text(" ");
	span
}

fn breakline() -> HtmlElement {
	utils::create_html_element("br")
}

fn word(slice: &str, tok: &Token) -> HtmlElement {
	let span = utils::create_html_element("span");
	span.set_class_name(tok.css_class());
	span.set_inner_text(slice);
	span
}
