use std::{env, fs::File, io::Write, path::PathBuf};

fn main() -> Result<(), Box<dyn std::error::Error>> {
	compile("index.scss", "index.css").unwrap();

	Ok(())
}

fn compile(input: &str, output: &str) -> Result<(), Box<dyn std::error::Error>> {
	let out_dir = env::var("OUT_DIR").unwrap();

	let output = PathBuf::from(out_dir).join(output);

	let mut output = File::create(output)?;

	let opts = grass::Options::default();
	let opts = opts.style(grass::OutputStyle::Compressed);

	let css_text = grass::from_path(input, &opts).unwrap_or_else(|e| {
		panic!("Sass Error: {e:#?}");
	});

	output.write_all(css_text.as_bytes())?;

	Ok(())
}
