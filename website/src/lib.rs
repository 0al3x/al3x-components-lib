#![recursion_limit = "256"]
use wasm_bindgen::prelude::*;

mod app;
mod code_snippets_example;

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
	wasm_logger::init(wasm_logger::Config::default());

	yew::Renderer::<app::App>::new().render();

	Ok(())
}
