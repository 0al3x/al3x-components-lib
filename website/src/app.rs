use crate::code_snippets_example;
use m3rs::yew::{
	prelude::buttons::elevated::ElevatedButton,
	typescale::{TypescaleModifier, TypescaleRoles},
	typography::Typography,
};
use wasm_bindgen::JsCast;
use web_sys::{HtmlStyleElement, MouseEvent};
use yew::{function_component, html, use_state, Callback, Html};

#[function_component(App)]
pub fn app() -> Html {
	set_base();
	m3rs::core::config().unwrap();
	config_css();

	html! { <>
		<Topbar />
		<div>
			<div class={"title"}>
				<div>
					<div><Typography
						text={"al3x's personal components lib"}
						role={TypescaleRoles::Display}
						modifier={TypescaleModifier::Large}
					/></div>
					<div><Typography
						text={"my personal library of web components written completely in rust"}
						role={TypescaleRoles::Headline}
						modifier={TypescaleModifier::Large}
					/></div>
					<div><Typography
						text={"all my components are under development and are provided without any warranty"}
						role={TypescaleRoles::Body}
						modifier={TypescaleModifier::Large}
					/></div>
				</div>
			</div>
		</div>
		<div>
			<code_snippets_example::CodeSnippets />
		</div>
	</> }
}

#[function_component(Topbar)]
pub fn topbar() -> Html {
	html! {<div class={"topbar"}>
		<ToggleDarkButton/>
	</div>}
}

#[function_component(ToggleDarkButton)]
pub fn toggle_dark_button() -> Html {
	let root = web_sys::window()
		.unwrap()
		.document()
		.unwrap()
		.query_selector(":root")
		.unwrap()
		.unwrap();

	let dark = use_state(|| root.class_name().ne("dark"));

	if *dark.clone() {
		let onclick = {
			let dark = dark.clone();
			Callback::from(move |_: MouseEvent| {
				root.set_class_name("dark");
				dark.set(false)
			})
		};

		html! { <div class={"toggle-btn"} >
			<ElevatedButton icon={"dark_mode"} label={"dark"} {onclick} />
		</div> }
	} else {
		let onclick = {
			let dark = dark.clone();
			Callback::from(move |_: MouseEvent| {
				root.set_class_name("light");
				dark.set(true)
			})
		};
		html! {<div class={"toggle-btn"} >
			<ElevatedButton icon={"light_mode"} label={"light"} {onclick} />
		</div>}
	}
}

fn set_base() {
	use wasm_bindgen::JsValue;
	use web_sys::{window, Document, HtmlBaseElement, Node};

	fn set_base() {
		let document = window().unwrap().document().unwrap();

		let base_elem =
			HtmlBaseElement::from(JsValue::from(document.create_element("base").unwrap()));

		base_elem.set_href(&origin(&document));

		let head = document.head().unwrap();

		head.append_child(&Node::from(base_elem)).unwrap();
	}

	fn origin(document: &Document) -> String {
		let location = document.location().unwrap();

		let origin = location.origin().unwrap();

		if location.hostname().unwrap().eq("localhost") {
			format!("{}/components-lib/", origin)
		} else {
			format!("{}/al3x-components-lib  ", origin)
		}
	}

	set_base();
}

fn config_css() {
	let style = web_sys::window()
		.unwrap()
		.document()
		.unwrap()
		.create_element("style")
		.unwrap()
		.dyn_into::<HtmlStyleElement>()
		.unwrap();

	web_sys::window()
		.unwrap()
		.document()
		.unwrap()
		.head()
		.unwrap()
		.append_child(&style)
		.unwrap();

	style.set_inner_html(include_str!(concat!(env!("OUT_DIR"), "/index.css")));
}
