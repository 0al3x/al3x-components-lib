use al3x_components_lib::code_snippets;
use m3rs::yew::{
	typescale::{TypescaleModifier, TypescaleRoles},
	typography::Typography,
};
use yew::{function_component, html, virtual_dom::VNode, Html};

#[function_component(CodeSnippets)]
pub fn code_snippets_example() -> Html {
	let input = code_snippets::Input {
		code: EXAMPLE_CODE,
		lang: code_snippets::Language::Rust,
	};

	let snip = {
		let snip = code_snippets::code_snnipet(input.clone());
		VNode::VRef(snip.into())
	};

	html! {<div class={"component-example"} >
		<div class={"example-header"} ><Typography
				text={"Code snippets"}
				role={TypescaleRoles::Display}
				modifier={TypescaleModifier::Small}
			/>
		</div>
		<div class={"example-headline"} ><Typography
			text={"Given a piece of code returns it with syntax highlight"}
			role={TypescaleRoles::Headline}
			modifier={TypescaleModifier::Large}
		/></div>
		{snip}
		<div class={"example-footer"} >
			<Typography
				text={"code: "}
				role={TypescaleRoles::Body}
				modifier={TypescaleModifier::Small}
			/>
			<a
				href={"https://gitlab.com/0al3x/al3x-components-lib/-/tree/master/code-snippets?ref_type=heads"}
				target={"_blank"}
			><Typography
				text={"https://gitlab.com/0al3x/al3x-components-lib/-/tree/master/code-snippets?ref_type=heads"}
				role={TypescaleRoles::Body}
				modifier={TypescaleModifier::Small}
			/></a>
		</div>
	</div>}
}

const EXAMPLE_CODE: &str = r#"#[function_component(App)]
pub fn app() -> Html {
	m3rs::core::config().unwrap();

	config_css();

	let code = "pub fn hello_world() {\n println!(\"hello world\"); \n}";

	let input = code_snippets::Input {
		code,
		lang: code_snippets::Language::Rust,
	};

	let snip = code_snippets::code_snnipet(input);

	VNode::VRef(snip.into())
}

fn config_css() {
	let style = web_sys::window()
		.unwrap()
		.document()
		.unwrap()
		.create_element("style")
		.unwrap()
		.dyn_into::<HtmlStyleElement>()
		.unwrap();

	web_sys::window()
		.unwrap()
		.document()
		.unwrap()
		.head()
		.unwrap()
		.append_child(&style)
		.unwrap();

	style.set_inner_html(include_str!(concat!(env!("OUT_DIR"), "/index.css")));
}"#;
